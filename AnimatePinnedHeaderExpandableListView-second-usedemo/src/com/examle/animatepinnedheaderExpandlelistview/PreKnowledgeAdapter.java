package com.examle.animatepinnedheaderExpandlelistview;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.common.view.AnimatedExpandableListAdapter;

public class PreKnowledgeAdapter extends AnimatedExpandableListAdapter {
	
	private Context mContext;
	private LayoutInflater mInflater;
	private List<String> mGroupList;
	private List<List<PreKnowledgeItemModel>> mChildList;

	public PreKnowledgeAdapter(Context context,List<String> groupList,
			 List<List<PreKnowledgeItemModel>> childList) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mGroupList = groupList;
		mChildList = childList;
	}

	// 返回父列表个数
	@Override
	public int getGroupCount() {
		return mGroupList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroupList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mChildList.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupHolder groupHolder = null;
		if (convertView == null) {
			groupHolder = new GroupHolder();
			convertView = mInflater.inflate(
					R.layout.item_preknowledge_group_exlist, null);
			groupHolder.titleTv = (TextView) convertView
					.findViewById(R.id.header_text);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (GroupHolder) convertView.getTag();
		}

		groupHolder.titleTv.setText(getGroup(groupPosition).toString());
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	private static class GroupHolder {
		TextView titleTv;
	}

	private static class ChildHolder {
		TextView contentWv;
	}
	@Override
	public View getRealChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ChildHolder childHolder = null;
		if (convertView == null) {
			childHolder = new ChildHolder();
			convertView = mInflater.inflate(R.layout.item_preknowledge_child_exlist, null);
			childHolder.contentWv = (TextView) convertView
					.findViewById(R.id.webview);
			convertView.setTag(childHolder);
		} else {
			childHolder = (ChildHolder) convertView.getTag();
		}
		String content = mChildList.get(groupPosition).get(childPosition).getContent();
		childHolder.contentWv.setText(Html.fromHtml(content));
		return convertView;
	}

	@Override
	public int getRealChildrenCount(int groupPosition) {
		return mChildList.get(groupPosition).size();
	}

}
