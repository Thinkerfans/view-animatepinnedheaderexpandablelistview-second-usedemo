package com.examle.animatepinnedheaderExpandlelistview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.common.view.AnimatePinnedHeaderExpandableListView;
import com.android.common.view.AnimatePinnedHeaderExpandableListView.OnHeaderUpdateListener;

public class MainActivity extends Activity implements
		OnHeaderUpdateListener, OnGroupClickListener {

	private AnimatePinnedHeaderExpandableListView mListView;
	private PreKnowledgeAdapter mAdapter;
	private ArrayList<String> mGroupList;
	private ArrayList<List<PreKnowledgeItemModel>> mchildList;
	private ViewGroup mHeaderView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pre_knowledge);

		initData();
		initView();

	}

	private void initData() {
		mGroupList = new ArrayList<String>();
		mchildList = new ArrayList<List<PreKnowledgeItemModel>>();

		String[] title = getResources().getStringArray(R.array.knowledge_title);
		String[] content = getResources().getStringArray(
				R.array.knowledge_content);
		
		for (int i = 0, size = title.length; i < size; i++) {
			mGroupList.add(title[i]);
			ArrayList<PreKnowledgeItemModel> list = new ArrayList<PreKnowledgeItemModel>();
			list.add(new PreKnowledgeItemModel(title[i], content[i]));
			mchildList.add(list);
		}

	}

	private void initView() {
		initListView();

	}

	private void initListView() {
		mListView = (AnimatePinnedHeaderExpandableListView) findViewById(R.id.listview);
		mAdapter = new PreKnowledgeAdapter(this, mGroupList,
				mchildList);

		mListView.setAdapter(mAdapter);
		mListView.setOnHeaderUpdateListener(this);
		mListView.setOnGroupClickListener(this);

	}



	@Override
	public View getPinnedHeader() {
		if (mHeaderView == null) {
			mHeaderView = (ViewGroup) getLayoutInflater().inflate(
					R.layout.item_preknowledge_group_exlist, null);
			mHeaderView.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}
		return mHeaderView;
	}

	@Override
	public void updatePinnedHeader(int firstVisibleGroupPos) {
		if (firstVisibleGroupPos != -1) {
			String title = (String) mAdapter.getGroup(firstVisibleGroupPos);
			TextView textView = (TextView) getPinnedHeader().findViewById(
					R.id.header_text);
			textView.setText(title);
		}
	}

	@Override
	public boolean onGroupClick(ExpandableListView parent, View v,
			int groupPosition, long id) {
		for (int i = 0, count = mListView.getCount(); i < count; i++) {
			if (mListView.isGroupExpanded(i) && i != groupPosition) {
				mListView.collapseGroupWithAnimation(i);
			}
		}

		if (mListView.isGroupExpanded(groupPosition)) {
			mListView.collapseGroupWithAnimation(groupPosition);
		} else {
			mListView.expandGroupWithAnimation(groupPosition);
		}
		return true;
	}
}
