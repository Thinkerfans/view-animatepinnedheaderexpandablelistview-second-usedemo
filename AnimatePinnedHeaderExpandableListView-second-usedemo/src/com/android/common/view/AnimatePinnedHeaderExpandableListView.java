package com.android.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

 /**
 * @author fanlitao
 */
public class AnimatePinnedHeaderExpandableListView extends ExpandableListView implements OnScrollListener {

	private static final String TAG = "PinnedHeaderExpandableListView";

	public interface OnHeaderUpdateListener {
		/**
		 * 采用单例模式返回同一个view对象即可 注意：view必须要有LayoutParams
		 */
		public View getPinnedHeader();

		public void updatePinnedHeader(int firstVisibleGroupPos);
	}

	private View mHeaderView;
	private int mHeaderWidth;
	private int mHeaderHeight;

	private OnScrollListener mScrollListener;
	private OnHeaderUpdateListener mHeaderUpdateListener;

	private boolean mActionDownHappened = false;
	
	public AnimatePinnedHeaderExpandableListView(Context context) {
		super(context);
		initView();
	}

	public AnimatePinnedHeaderExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public AnimatePinnedHeaderExpandableListView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	private void initView() {
		setFadingEdgeLength(0);
		setOnScrollListener(this);
	}
	@Override
	public void setOnScrollListener(OnScrollListener l) {
		if (l != this) {
			mScrollListener = l;
		}
		super.setOnScrollListener(this);
	}

	public void setOnHeaderUpdateListener(OnHeaderUpdateListener listener) {
		mHeaderUpdateListener = listener;
		if (listener == null) {
			return;
		}
        mHeaderView = listener.getPinnedHeader();
        int firstVisiblePos = getFirstVisiblePosition();       
        int firstVisibleGroupPos = getPackedPositionGroup(getExpandableListPosition(firstVisiblePos));
        listener.updatePinnedHeader(firstVisibleGroupPos);
        requestLayout();
        postInvalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mHeaderView == null) {
			return;
		}
		measureChild(mHeaderView, widthMeasureSpec, heightMeasureSpec);
		mHeaderWidth = mHeaderView.getMeasuredWidth();
		mHeaderHeight = mHeaderView.getMeasuredHeight();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if (mHeaderView == null) {
			return;
		}
		refreshHeader();
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		if (mHeaderView != null) {
			drawChild(canvas, mHeaderView, getDrawingTime());
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		int x = (int) ev.getX();
		int y = (int) ev.getY();
		int pos = pointToPosition(x, y);
		if (y >= mHeaderView.getTop() && y <= mHeaderView.getBottom()) {
			if (ev.getAction() == MotionEvent.ACTION_DOWN) {
				mActionDownHappened = true;
			} else if (ev.getAction() == MotionEvent.ACTION_UP) {
				int groupPosition = getPackedPositionGroup(getExpandableListPosition(pos));			
				if (groupPosition != INVALID_POSITION && mActionDownHappened) {					

                    for (int i = 0, count = this.getCount(); i < count; i++) {
                    	if(isGroupExpanded(i) && i!=groupPosition){
                    		collapseGroupWithAnimation(i);
                    	}
                    }
                    
                    if (isGroupExpanded(groupPosition)) {
                        collapseGroupWithAnimation(groupPosition);
                    } else {
            			expandGroupWithAnimation(groupPosition);
                    }

				}
			}
		}

		return super.dispatchTouchEvent(ev);
	}

	protected void refreshHeader() {
		if (mHeaderView == null) {
			return;
		}

        int firstVisiblePos = getFirstVisiblePosition();
        int pos = firstVisiblePos + 1;
        int firstVisibleGroupPos = getPackedPositionGroup(getExpandableListPosition(firstVisiblePos));
        int group = getPackedPositionGroup(getExpandableListPosition(pos));
	
		if (group == firstVisibleGroupPos + 1) {
			View view = getChildAt(1);
			if (view != null && view.getTop() <= mHeaderHeight) {
				int delta = mHeaderHeight - view.getTop();
				mHeaderView.layout(0, -delta, mHeaderWidth, mHeaderHeight
						- delta);
			}else {
				mHeaderView.layout(0, 0, mHeaderWidth, mHeaderHeight);
			}
		} else {
			mHeaderView.layout(0, 0, mHeaderWidth, mHeaderHeight);
		}

		if (mHeaderUpdateListener != null) {
			mHeaderUpdateListener.updatePinnedHeader(firstVisibleGroupPos);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (mHeaderView != null && scrollState == SCROLL_STATE_IDLE) {
			refreshHeader();
		}
		if (mScrollListener != null) {
			mScrollListener.onScrollStateChanged(view, scrollState);
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (mHeaderView != null && totalItemCount > 0) {
			refreshHeader();
		}
		if (mScrollListener != null) {
			mScrollListener.onScroll(view, firstVisibleItem, visibleItemCount,
					totalItemCount);
		}
	}
    private static final int ANIMATION_DURATION = 500;
    
    private AnimatedExpandableListAdapter adapter;
    
    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);        
        // Make sure that the adapter extends AnimatedExpandableListAdapter
        if(adapter instanceof AnimatedExpandableListAdapter) {
            this.adapter = (AnimatedExpandableListAdapter) adapter;
            this.adapter.setParent(this);
        } else {
            throw new ClassCastException(adapter.toString() + " must implement AnimatedExpandableListAdapter");
        }
    }
    
    
    /**
     * Expands the given group with an animation.
     * @param groupPos The position of the group to expand
     * @return  Returns true if the group was expanded. False if the group was
     *          already expanded.
     */
    public boolean expandGroupWithAnimation(int groupPos) {
        int groupFlatPos = getFlatListPosition(getPackedPositionForGroup(groupPos));
        if (groupFlatPos != -1) {
            int childIndex = groupFlatPos - getFirstVisiblePosition();
            if (childIndex < getChildCount()) {
                // Get the view for the group is it is on screen...
                View v = getChildAt(childIndex);
                if (v.getBottom() >= getBottom()) {
                    // If the user is not going to be able to see the animation
                    // we just expand the group without an animation.
                    // This resolves the case where getChildView will not be
                    // called if the children of the group is not on screen
                    
                    // We need to notify the adapter that the group was expanded
                    // without it's knowledge
                    adapter.notifyGroupExpanded(groupPos);
                    return expandGroup(groupPos);
                }
            }
        }
        
        // Let the adapter know that we are starting the animation...
        adapter.startExpandAnimation(groupPos, 0);
        // Finally call expandGroup (note that expandGroup will call
        // notifyDataSetChanged so we don't need to)
        return expandGroup(groupPos);
    }
    
    /**
     * Collapses the given group with an animation.
     * @param groupPos The position of the group to collapse
     * @return  Returns true if the group was collapsed. False if the group was
     *          already collapsed.
     */
    public boolean collapseGroupWithAnimation(int groupPos) {
        int groupFlatPos = getFlatListPosition(getPackedPositionForGroup(groupPos));
        if (groupFlatPos != -1) {
            int childIndex = groupFlatPos - getFirstVisiblePosition();
            if (childIndex >= 0 && childIndex < getChildCount()) {
                // Get the view for the group is it is on screen...
                View v = getChildAt(childIndex);
                if (v.getBottom() >= getBottom()) {
                    // If the user is not going to be able to see the animation
                    // we just collapse the group without an animation.
                    // This resolves the case where getChildView will not be
                    // called if the children of the group is not on screen
                    return collapseGroup(groupPos);
                }
            } else {
                // If the group is offscreen, we can just collapse it without an
                // animation...
                return collapseGroup(groupPos);
            }
        }
        
        // Get the position of the firstChild visible from the top of the screen
        long packedPos = getExpandableListPosition(getFirstVisiblePosition());
        int firstChildPos = getPackedPositionChild(packedPos);
        int firstGroupPos = getPackedPositionGroup(packedPos);
        
        // If the first visible view on the screen is a child view AND it's a
        // child of the group we are trying to collapse, then set that
        // as the first child position of the group... see
        // {@link #startCollapseAnimation(int, int)} for why this is necessary
        firstChildPos = firstChildPos == -1 || firstGroupPos != groupPos ? 0 : firstChildPos;
        
        // Let the adapter know that we are going to start animating the 
        // collapse animation.
        adapter.startCollapseAnimation(groupPos, firstChildPos);
        
        // Force the listview to refresh it's views
        adapter.notifyDataSetChanged();
        return isGroupExpanded(groupPos);
    }
    
    public int getAnimationDuration() {
        return ANIMATION_DURATION;
    }
}