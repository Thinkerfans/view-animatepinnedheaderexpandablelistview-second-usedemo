package com.examle.animatepinnedheaderExpandlelistview;

public class PreKnowledgeItemModel {
	
	private String title;
	private String content;	
	
	public PreKnowledgeItemModel(String title, String url) {
		this.title = title;
		this.content = url;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String url) {
		this.content = url;
	}
}
